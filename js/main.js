$(function() {
  $('.post-link-item .slider').bxSlider({
    controls: false,
    pager: false
  });

  $('.copy-image ul').bxSlider({
    adaptiveHeight: true
  });

  var contentHeight = $(window).height() - $('.header').height();
  // $('.content, .content-right, .content-left .hero, .scroll').height(contentHeight);
  $('.content, .hero, .scroll').each(function() {
    $(this).height(contentHeight);
  });

  $('.product-item .content-right').each(function() {
    $(this).css('min-height', contentHeight);
  });

  var jScrollPaneSettings = {
    contentWidth: '0px',
    verticalGutter: 0,
    hideFocus: true
  };

  $(window).on('load resize', function() {

    $('.scroll').jScrollPane(jScrollPaneSettings);

    // Same height for Social Feed items
    $('.social-feed').each(function() {
      var maxHeight = 0;
      var $socialFeedItems = $(this).find('li');

      $socialFeedItems.each(function() {
        if ($(this).height() > maxHeight) {
          maxHeight = $(this).height();
        }
      });

      $socialFeedItems.each(function() {
        $(this).height(maxHeight);
      });

      // Set last-child height minus button height and adapt button
      // to last-child width and height
      var $socialFeedBtn = $(this).find('.btn');
      $socialFeedItems.last().height(maxHeight - $socialFeedBtn.height() - 14);
      $socialFeedBtn.width($socialFeedItems.last().width() + 2);

    });

    $('.copy-bg').each(function() {
      var img = $(this).find('img');
      $(this).css('background-image', 'url(' + img.attr('src') + ')');
      $(this).height(img.height());
      img.hide();
    });

    // Animate to footer when scroll is at bottom
    $('.scroll').on('jsp-arrow-change', function(event, isAtTop, isAtBottom) {
      if (isAtBottom) {
        $('body').addClass('show-footer')
        $('body').removeClass('hide-footer')
      }
      else {
        $('body').removeClass('show-footer')
        $('body').addClass('hide-footer')
      }
    });

    $('.inspirationals-item, .product-item').each(function() {
      var $current = $(this);
      var p = $current.position().top;

      $current.scrollspy({
        container: '.scroll',
        min: p,
        max: p + $current.height(),
        onEnter: function() {
          $current.find('.hero').addClass('fixed');
        },
        onLeave: function() {
          $current.find('.hero').removeClass('fixed');
        }
      });
    });

    $('.article-index-set').each(function() {
      var $current = $(this);
      var p = $current.position().top;

      $current.scrollspy({
        container: '.scroll',
        min: p,
        max: $current.next().offset().top - $current.find('.hero:eq(0)').height() - 70,
        onEnter: function() {
          $current.find('.hero').addClass('fixed').css('top', '');
        },
        onLeave: function() {
          var currentScroll = $('.scroll').data('jsp').getContentPositionY();
          $current.find('.hero').removeClass('fixed').css('top', currentScroll);
        }
      });
    });

    if ($('.tag-wall').length) {
      $('.container').addClass('full-width');
      $('html, body').addClass('show-overflow');
      $('.content').each(function() {
        $(this).height('');
        $(this).css('min-height', contentHeight - $('.footer').height());
      });
    }

    $('.tag-wall .content-full ul').masonry({
      itemSelector: 'li',
      gutter: 8,
      isFitWidth: true
    });


  });

  function timer(date, selector) {
    var end = new Date(date);

    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    var html;

    function showRemaining() {
      var now = new Date();
      var distance = end - now;
      if (distance < 0) {

        clearInterval(timer);
        $(selector).html('<li>00</li><li>00</li><li>00</li><li>00</li>');

        return;
      }
      var days = Math.floor(distance / _day);
      var hours = Math.floor((distance % _day) / _hour);
      var minutes = Math.floor((distance % _hour) / _minute);
      var seconds = Math.floor((distance % _minute) / _second);

      html = '<li>' + days + '</li>' +
                 '<li>' + hours + '</li>' +
                 '<li>' + minutes + '</li>' +
                 '<li>' + seconds + '</li>';

      $(selector).html(html);
    }

    setInterval(showRemaining, 1000);
    showRemaining();
  }

  timer('02/22/2016 10:01 AM', '.timer-count');


});